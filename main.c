//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------
// School: University of Victoria, Canada.
// Course: CENG 355 "Microprocessor-Based Systems".
//
// See "system/include/cmsis/stm32f0xx.h" for register/bit definitions.
// See "system/src/cmsis/vectors_stm32f0xx.c" for handler declarations.
// ----------------------------------------------------------------------------

#include <stdio.h>
#include "diag/Trace.h"
#include "cmsis/cmsis_device.h"

// ----------------------------------------------------------------------------
//
// STM32F0 empty sample (trace via $(trace)).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the $(trace) output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

/* Clock prescaler for TIM2 timer: no prescaling */
#define myTIM2_PRESCALER ((uint16_t)0x0000)
/* Maximum possible setting for overflow */
#define myTIM2_PERIOD ((uint32_t)0xFFFFFFFF)
/* Define LCK pin as pin PB4 for BSRR and BRR */
#define LCK_pin ((uint16_t)0x0010);

/* Defines for SPI output */
#define LCD_EN ((uint8_t)0x80)
#define RS_mask ((uint8_t)0x40)

/* LCD Modes */
#define INSTRUCTION ((uint8_t)0x00)
#define CHARACTER ((uint8_t)0x01)

/* Define LCD Instruction Modes */
#define LCD_Clear_Display ((uint8_t)0x01)
#define LCD_Cursor_Home ((uint8_t)0x02)
#define LCD_Entry_Mode_Set ((uint8_t)0x04)
#define LCD_Display_Control ((uint8_t)0x08)
#define LCD_Cursor_Set ((uint8_t)0x10)
#define LCD_Function_Set ((uint8_t)0x20)
#define LCD_Set_DDRAM ((uint8_t)0x80)

/* Defines for LCD instruction bits */
#define LCD_ID ((uint8_t)0x02)
#define LCD_S ((uint8_t)0x01)
#define LCD_D ((uint8_t)0x04)
#define LCD_C ((uint8_t)0x02)
#define LCD_B ((uint8_t)0x01)
#define LCD_SC ((uint8_t)0x08)
#define LCD_RL ((uint8_t)0x04)
#define LCD_DL ((uint8_t)0x10)
#define LCD_N ((uint8_t)0x08)
#define LCD_F ((uint8_t)0x04)
#define LCD_4bit ((uint8_t)0x02)

#define L1_Address ((uint8_t)0x00)
#define L2_Address ((uint8_t)0x40)

/* Define LCD masks */
#define high_bit_mask ((uint8_t)0xF0)
#define low_bit_mask ((uint8_t)0x0F)

/* Define LCD characters */
#define LCD_char_F ((uint8_t)0b01000110)
#define LCD_char_R ((uint8_t)0b01010010)
#define LCD_char_colon ((uint8_t)0b00111010)
#define LCD_char_H ((uint8_t)0b01001000)
#define LCD_char_z ((uint8_t)0b01111010)
#define LCD_char_O ((uint8_t)0b01001111)
#define LCD_char_h ((uint8_t)0b01101000)
#define LCD_decimal_offset ((uint8_t)0x30)

/* Define the struct holding Initialization Settings for SPI */
SPI_InitTypeDef SPI_InitStructInfo;
SPI_InitTypeDef* SPI_InitStruct = &SPI_InitStructInfo;

void myGPIOA_Init(void);
void myGPIOB_Init(void);
void myGPIOC_Init(void);
void myTIM2_Init(void);
void myEXTI_Init(void);
void TM_DelayMicro(uint32_t micro);
void TM_DelayMillis(uint32_t millis);
void myADC_Init(void);
void myDAC_Init(void);
void mySPI_Init(void);
void myLCD_Init(void);
void mySendSPI(uint8_t spi_data);
void mySendLCD(uint8_t spi_data, uint8_t RS);
int myResConvert(uint16_t adc_reading);
void myUpdateFreq(uint16_t freq);
void myUpdateRes(uint16_t res);

// Global Constants
uint16_t ADC_RES = 4096;
uint16_t POT_RATING = 5000;
uint16_t SYS_VOLT_MILLI = 3300;

// Global variables
uint16_t firstEdgeDetected;
uint16_t resistance = 0;
uint16_t adc_reading = 0;
uint16_t frequency = 0;

int main(int argc, char* argv[])
{

	myGPIOA_Init();		/* Initialize I/O port A */
	myGPIOB_Init(); 	/* Initialize I/O port B */
	myGPIOC_Init(); 	/* Initialize I/O port C */
	myADC_Init();		/* Initialize ADC for operation */
	myDAC_Init();		/* Initialize DAC for operation */
	mySPI_Init();		/* Initialize SPI1 for operation */
	myTIM2_Init();		/* Initialize timer TIM2 */
	myEXTI_Init();		/* Initialize EXTI */
	TM_DelayMillis(50); /* Allow time for voltage to stabilize in LCD */
	myLCD_Init();		/* Initialize LCD interface */

	while (1)
	{
		/* Start ADC conversion */
		// Relevant register: ADC1->CR bit ADSTART
		ADC1->CR |= ADC_CR_ADSTART;

		/* Wait until conversion is done */
		// Relevant register: ADC1->ISR, EOC bit
		while(!(ADC1->ISR & ADC_ISR_EOC));

		/* Get digital value from ADC and put in PC1_analog_data */
		// Relevant register: ADC1->DR
		adc_reading = ADC1->DR;

		/* Send DAC data from PC1_analog_data */
		// Relevant register: DAC_DHR12Rx
		DAC->DHR12R1 = DAC_DHR12R1_DACC1DHR & resistance;

		/* Send resistance value to LCD */
		resistance = myResConvert(adc_reading);
		myUpdateRes(resistance);

		/* Send frequency value to LCD */
		myUpdateFreq(frequency);

		/* Limit screen updates to every 1/5 of a second) */
		TM_DelayMillis(200);

	}

	return 0;

}

int myResConvert(uint16_t adc_reading)
{
	/*  This function takes the ADC reading from the potentiometer and calculates the
	 *  resistance of the potentiometer */

	/* Convert the ADC reading into the corresponding voltage in millivolts */
	// voltage_millis = (adc_reading * SYS_VOLT_MILLI) / ADC_RES;
	// resistance = (POT_RATING * voltage_millis) / SYS_VOLT_MILLI;
	/* Calculate resistance of potentiometer from measured voltage */

	return (POT_RATING * adc_reading) / ADC_RES;
}

void myUpdateFreq(uint16_t freq)
{
	/* This function takes the frequency and updates the LCD display */

	/* Declare local variables: */
	uint16_t MAX_FREQ = 9999;
	uint8_t freq_thous, freq_hunds, freq_tens, freq_ones;

	/* First trim the frequency to the right format */
	if(freq > MAX_FREQ)
		freq = MAX_FREQ;

	/* Break the frequency trimmed value into digits */
	freq_thous = (freq / 1000);
	freq_hunds = (freq % 1000) / 100;
	freq_tens = (freq % 100) / 10;
	freq_ones = (freq % 10);

	/* Send Instruction: SET DDRAM to First Line first Char */
	mySendLCD(LCD_Set_DDRAM|L1_Address, INSTRUCTION);
	/* Send "F" to LCD display */
	mySendLCD(LCD_char_F, CHARACTER);
	/* Send ":" to LCD display */
	mySendLCD(LCD_char_colon, CHARACTER);
	/* Send freq_thous to LCD display */
	mySendLCD((uint8_t)(LCD_decimal_offset + freq_thous), CHARACTER);
	/* Send freq_hunds to LCD display */
	mySendLCD((uint8_t)(LCD_decimal_offset + freq_hunds), CHARACTER);
	/* Send freq_tens to LCD display */
	mySendLCD((uint8_t)(LCD_decimal_offset + freq_tens), CHARACTER);
	/* Send freq_ones to LCD display */
	mySendLCD((uint8_t)(LCD_decimal_offset + freq_ones), CHARACTER);
	/* Send "H" to LCD display */
	mySendLCD(LCD_char_H, CHARACTER);
	/* Send "z" to LCD display */
	mySendLCD(LCD_char_z, CHARACTER);

	/* finished update frequency to LCD display */

}

void myUpdateRes(uint16_t res)
{
	/* This function displays the resistance on the LCD display */

	/* Declare local variables: */
	uint8_t res_thous;
	uint8_t res_hunds;
	uint8_t res_tens;
	uint8_t res_ones;

	/* Break the res value into digits */
	res_thous = res / 1000;
	res_hunds = (res % 1000) / 100;
	res_tens = (res % 100) / 10;
	res_ones = (res % 10);

	/* Send Instruction: SET DDRAM to Second Line, First Char */
	mySendLCD(LCD_Set_DDRAM|L2_Address, INSTRUCTION);
	/* Send "F" to LCD display */
	mySendLCD(LCD_char_R, CHARACTER);
	/* Send ":" to LCD display */
	mySendLCD(LCD_char_colon, CHARACTER);
	/* Send res_thous to LCD display */
	mySendLCD((uint8_t)(LCD_decimal_offset + res_thous), CHARACTER);
	/* Send res_hunds to LCD display */
	mySendLCD((uint8_t)(LCD_decimal_offset + res_hunds), CHARACTER);
	/* Send res_tens to LCD display */
	mySendLCD((uint8_t)(LCD_decimal_offset + res_tens), CHARACTER);
	/* Send res_ones to LCD display */
	mySendLCD((uint8_t)(LCD_decimal_offset + res_ones), CHARACTER);
	/* Send "H" to LCD display */
	mySendLCD(LCD_char_O, CHARACTER);
	/* Send "z" to LCD display */
	mySendLCD(LCD_char_h, CHARACTER);

}


void myGPIOA_Init()
{
	/* Enable clock for GPIOA peripheral */
	// Relevant register: RCC->AHBENR
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	/* Configure PA1 as input */
	// Relevant register: GPIOA->MODER
	GPIOA->MODER &=~ (GPIO_MODER_MODER1);

	/* Configure PA4 as analog output for DAC */
	// Relevant register: GPIOA->MODER
	GPIOA->MODER &=~ (GPIO_MODER_MODER4);
	GPIOA->MODER |= (GPIO_MODER_MODER4);

	/* Ensure no pull-up/pull-down for PA1 and PA4 */
	// Relevant register: GPIOA->PUPDR
	GPIOA->PUPDR &=~ (GPIO_PUPDR_PUPDR1 | GPIO_PUPDR_PUPDR4);
}

void myGPIOB_Init()
{
	/* Enable clock for GPIOB peripheral */
	// Relevant register: RCC->AHBENR
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	/* Configure PB3 and PB5 as AF and PB4 as general purpose output*/
	// PB3 is SCK and PB5 is MOSI
	// PB4 is LCK
	// Relevant register: GPIOB->MODER bit MODER
	GPIOB->MODER |= (GPIO_MODER_MODER3_1 | GPIO_MODER_MODER4_0 | GPIO_MODER_MODER5_1);

	/* Ensure no pull-up/pull-down for PB3, PB4 and PB5 */
	// Relevant register: GPIOA->PUPDR
	GPIOB->PUPDR &=~ (GPIO_PUPDR_PUPDR3 | GPIO_PUPDR_PUPDR4 | GPIO_PUPDR_PUPDR5);
}


void myGPIOC_Init()
{
	/* Enable clock for GPIOC peripheral */
	// Relevant register: RCC->AHBENR
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;

	/* Configure PC1 as analog input of ADC */
	// Relevant register: GPIOC->MODER
	GPIOC->MODER &= ~(GPIO_MODER_MODER1);
	GPIOC->MODER |= (GPIO_MODER_MODER1);


	/* Ensure no pull-up/pull-down for PC1 */
	// Relevant register: GPIOA->PUPDR
	GPIOC->PUPDR &=~ (GPIO_PUPDR_PUPDR1);
}


void myADC_Init()
{
	/* Enable clock for ADC */
	// Relevant register: RCC->APB2ENR
	RCC->APB2ENR |= RCC_APB2ENR_ADCEN;

	/* Calibrate ADC, ADCAL = 1 */
	//Relevant register ADC1->CR bit ADCAL
	ADC1->CR |= ADC_CR_ADCAL;

	// Wait until ADCAL resets to zero for calibration to be finished
	while ((ADC1->CR & ADC_CR_ADCAL));

	/* Configure ADC */
	// Select channel 0 to use for ADC, PC1.
	ADC1->CHSELR |= ADC_CHSELR_CHSEL11;

	/* Set over run to update DR with new data */
	/* Set ADC to continuous conversion mode, CONT = 1; */
	// Relevant register: ADC1->CFGR1
	ADC1->CFGR1 = ADC_CFGR1_OVRMOD | ADC_CFGR1_CONT;

	/* Set ADC sampling time to 239.5 clock cycles */
	// Relevant register: ADC1->SMPR
	ADC1->SMPR |= ADC_SMPR_SMP;

	/* Enable ADC */
	// Relevant register: ADC1->CR bit ADEN
	ADC1->CR |= ADC_CR_ADEN;
	while(!(ADC1->ISR & ADC_CR_ADEN));

	/* ADC has been configured */
}

void myDAC_Init()
{
	/* Enable clock for DAC */
	// Relevant register: RCC->APB1ENR
	RCC->APB1ENR |= RCC_APB1ENR_DACEN;

	/* Enable DAC */
	// Relevant register: DAC->CR EN1 bit
	DAC->CR |= DAC_CR_EN1;

}

void mySPI_Init()
{
	/* Enable clock for SPI interface, directly taken from course notes */
	// Relevant register RCC->APB2ENR
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;

	SPI_InitStruct->SPI_Direction = SPI_Direction_1Line_Tx;
	SPI_InitStruct->SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct->SPI_DataSize= SPI_DataSize_8b;
	SPI_InitStruct->SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStruct->SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStruct->SPI_NSS = SPI_NSS_Soft;
	SPI_InitStruct->SPI_BaudRatePrescaler=	SPI_BaudRatePrescaler_256;
	SPI_InitStruct->SPI_FirstBit=SPI_FirstBit_MSB;
	SPI_InitStruct->SPI_CRCPolynomial= 7;
	SPI_Init(SPI1, SPI_InitStruct);

	SPI_Cmd(SPI1, ENABLE);

}

void mySendSPI(uint8_t spi_data)
{

	/* Force LCK signal to 0 */
	/* Write 0 to PB4 */
	// Relevant register: GPIOB->BSRR bit BS_4
	GPIOB->BRR = LCK_pin;

	/* Wait for buffer to be empty */
	// TXE = 1 when buffer is empty
	while(!(SPI1->SR & SPI_SR_TXE));

	/* Wait for SPI to be not busy */
	// BSY = 0 when SPI is not busy
	while((SPI1->SR & SPI_SR_BSY));

	/* Assumption: your data holds 8 bits to be sent */
	SPI_SendData8(SPI1, spi_data);

	/* Wait until SPI1 is not busy (BSY = 0) */
	// Relevant register: SPI1-SR bit BSY
	while((SPI1->SR & SPI_SR_BSY));

	/* Force your LCK signal to 1 */
	/* Write 1 to PB4 */
	// Relevant register: GPIOB->BSRR bit BR_4
	GPIOB->BSRR = LCK_pin;

	/* Delay about 18 microseconds */
	TM_DelayMicro(18);


}

void mySendLCD(uint8_t spi_data, uint8_t RS)
{
	uint8_t highBits, lowBits;

	/*  RS bit determines the mode
	 *  RS=0 Instruction Mode
	 *  RS=1 Character Mode */

	/*	Move RS LSB (xxxx xxxX) to bit 6 (0X00 0000)
	 *	Ensure all non-RS bits are zero  */
	RS = RS << 6;
	RS = (RS & RS_mask);

	/*Split INPUT data HHHHLLLL into high 0000HHHH and low 0000LLLL bits */
	highBits = ((spi_data & high_bit_mask) >> 4);
	lowBits = (spi_data & low_bit_mask);

	/* SEND HIGH BITS */
	// Send 0RxxHHHH
	mySendSPI(RS|highBits);
	TM_DelayMicro(10);
	// Send 1RxxHHHH
	mySendSPI(LCD_EN|RS|highBits);
	TM_DelayMicro(10);
	// Send 0RxxHHHH
	mySendSPI(RS|highBits);
	TM_DelayMicro(40);

	/* SEND LOW BITS*/
	// Send 0RxxLLLL
	mySendSPI(RS|lowBits);
	TM_DelayMicro(10);
	// Send 1RxxLLLL
	mySendSPI(LCD_EN|RS|lowBits);
	TM_DelayMicro(10);
	// Send 0RxxLLLL
	mySendSPI(RS|lowBits);
	TM_DelayMicro(40);

	TM_DelayMillis(5);
}

void myLCD_Init()
{
	/* LCD Initialization Sequence
	 * Here we get the LCD ready to receive inputs and commands */

	/* Initialize to 4-bit interface
	 * Send  0   0     0010   0000
	 *       RS  R/W   H-bit  L-bits
	 * Low bit's tied to ground, really send 0000 0010
	 * after this the 4-bit interface is in effect
	 * send 00xx0010, 10xx0010, 00xx0010 */

	mySendSPI(LCD_4bit); // send without enable
	TM_DelayMicro(10);
	mySendSPI(LCD_EN|LCD_4bit); // send with enable
	TM_DelayMicro(10);
	mySendSPI(LCD_4bit); // send without enable
	TM_DelayMillis(5);

	/* Once in 4-bit mode, character and control data are transferred
	 * as pairs of 4-bit "nibbles" on the upper data pins, D4-D7. */

	/* Function Set
	 * Keep 4-bit interface, set display to 2 lines, set 5x8 chars
	 * Function set 00 0010  1000
	 * 				      DL NF
	 * RS=0 (command)
	 * DL=0 (4-bit interface)
	 * N=1 (two lines)
	 * F=0 (5x8 dots)*/
	mySendLCD(LCD_Function_Set|LCD_N, INSTRUCTION);

	/* Display Control
	 * Display on, cursor displayed and blinking
	 * Function set 00 0000 1100
	 * 				         DCB
	 * RS=0 (command)
	 * D=1 (Display On)
	 * C=0 (Cursor Displayed)
	 * B=0 (Cursor Blinking)*/
	mySendLCD(LCD_Display_Control|LCD_D, INSTRUCTION);

	/* Entry Mode Set
	 * Auto Increment cursor, display not shifted
	 * Function set 00 0000 0110
	 * 				        I/DS
	 * RS=0 (command)
	 * I/D=1 (Auto increment after char)
	 * S=0   (Display not shifted)*/
	mySendLCD(LCD_Entry_Mode_Set|LCD_ID, INSTRUCTION);

	/* Clear Display, Return Home Instruction
	 * RS=0 (instruction mode)
	 * DB7-0: 0000 0001 */
	mySendLCD(LCD_Clear_Display, INSTRUCTION);

	TM_DelayMillis(5);
}


void TM_DelayMicro(uint32_t micro)
{
	/* Multiply millis by 8 */
	/* subtract 10 */
	micro = (micro * 8) - 10;
	/* 4 cycles for one loop */
	while(micro--)
	{
		asm("");
	}
}

void TM_DelayMillis(uint32_t millis)
{
	/*  Multiply millis by 8 */
	/* 	Subtract 10 */
	millis = (1000 * millis * 8) - 10;
	/* 4 cycles for one loop */
	while(millis--)
	{
		asm("");
	}
}

void myTIM2_Init()
{
	/* Enable clock for TIM2 peripheral
	 * Relevant register: RCC->APB1ENR */

	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	/* Configure TIM2: buffer auto-reload, count up, stop on overflow,
	 * enable update events, interrupt on overflow only
	 * Relevant register: TIM2->CR1
	 * bit 2 and bit 1 are unclear pg 389 refman
	 * xxxx xxxx 1xx0 1100
	 * ones: 	0x0089
	 * zeros: 	0x0013 */

	TIM2->CR1 = ((uint16_t)0x008C);

	/* Set clock prescaler value */
	TIM2->PSC = myTIM2_PRESCALER;
	/* Set auto-reloaded delay */
	TIM2->ARR = myTIM2_PERIOD;

	/* Update timer registers */
	// Relevant register: TIM2->EGR
	TIM2->EGR = ((uint16_t)0x0001);

	/* Assign TIM2 interrupt priority = 0 in NVIC
	 * Relevant register: NVIC->IP[3], or use NVIC_SetPriority
	 */
	NVIC_SetPriority(TIM2_IRQn, 0);

	/* Enable TIM2 interrupts in NVIC */
	// Relevant register: NVIC->ISER[0], or use NVIC_EnableIRQ
	NVIC_EnableIRQ(TIM2_IRQn);

	/* Enable update interrupt generation */
	// Relevant register: TIM2->DIER
	TIM2->DIER |= TIM_DIER_UIE;

}

void myEXTI_Init()
{
	/* Map EXTI1 line to PA1
	 * Relevant register: SYSCFG->EXTICR[0]
	 * xxxx xxxx  0000 xxxx pg 172 refman
	 */

	SYSCFG->EXTICR[0] &=~ ((uint16_t)0x000000F0);

	/* EXTI1 line interrupts: set rising-edge trigger
	 * Relevant register: EXTI->RTSR
	 * xxxx xxxx xxxx xxxx xxxx xxxx xxxx xx1x = 0x00000002
	 * pg 200 refman. line TR1 set to rising trigger
	 */
	EXTI->RTSR |= ((uint16_t)0x00000002);

	/* Un-mask interrupts from EXTI1 line
	 * Relevant register: EXTI->IMR
	 * same as above except fot the IMR
	 * pg 199 refman
	 */
	EXTI->IMR |= ((uint16_t)0x00000002);

	/* Assign EXTI1 interrupt priority = 0 in NVIC */
	// Relevant register: NVIC->IP[1], or use NVIC_SetPriority
	// pg 194 refman
	NVIC_SetPriority(EXTI0_1_IRQn, 0);

	/* Enable EXTI1 interrupts in NVIC */
	// Relevant register: NVIC->ISER[0], or use NVIC_EnableIRQ
	NVIC_EnableIRQ(EXTI0_1_IRQn);
}


/* This handler is declared in system/src/cmsis/vectors_stm32f0xx.c */
void TIM2_IRQHandler()
{
	/* Check if update interrupt flag is indeed set */
	if ((TIM2->SR & TIM_SR_UIF) != 0)
	{
		trace_printf("\n*** Overflow! ***\n");

		/* Clear update interrupt flag */
		// Relevant register: TIM2->SR
		TIM2->SR &=~ (TIM_SR_UIF);

		/* Restart stopped timer */
		// Relevant register: TIM2->CR1
		TIM2->CR1 |= ((uint16_t)0x0001);
	}
}


/* This handler is declared in system/src/cmsis/vectors_stm32f0xx.c */
void EXTI0_1_IRQHandler()
{
	/* Local variables */
	uint32_t timerValue = 0;

	/* Check if EXTI1 interrupt pending flag is indeed set */
	if ((EXTI->PR & EXTI_PR_PR1) != 0)
	{
		// 1. If this is the first edge:
		if (firstEdgeDetected == 0){
			//	- Clear count register (TIM2->CNT).
			// pg 404 refman, 0x00000000 to reset
			TIM2->CNT = ((uint32_t)0x0);

			//	- Start timer (TIM2->CR1).
			TIM2->CR1 |= ((uint16_t)0x0001);
			firstEdgeDetected = ((uint16_t)0x0001);
		}

		// 2. If this is not the first edge:
		else{	//    Else (this is the second edge):
			//	- Stop timer (TIM2->CR1).
			TIM2->CR1 &=~ ((uint16_t)0x0001);

			//	- Read out count register (TIM2->CNT).
			timerValue = (uint32_t)TIM2->CNT;

			//  - Disable port A interrupt
			NVIC_DisableIRQ(EXTI0_1_IRQn);

			//  - Reset firstEdgeDetected to 0
			firstEdgeDetected = 0;

			//	- Calculate signal frequency and period.
			frequency = SystemCoreClock / timerValue;

			//  - Print values to console.
			//trace_printf("frequency: %u Hz\n", frequency);
			//trace_printf("period: %u microseconds\n", period);

			/*	  NOTE: Function trace_printf does not work
			 *	  with floating-point numbers: you must use
			 *	  "unsigned int" type to print your signal
			 *	  period and frequency. */

			NVIC_EnableIRQ(EXTI0_1_IRQn);

			//  - Reset timerValue to zero.
			timerValue = 0;
		}

		// 3. Clear EXTI1 interrupt pending flag (EXTI->PR).
		// Clear the flag by writing a 1, ref man page 202
		EXTI->PR |= (EXTI_PR_PR1);

	}
	else{
		trace_printf("error");
	}
}


#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
